extends Node2D

signal reset
signal start(player: int)

var points_left: int = 0
var points_right: int = 0
var start_text: String = "Gegen wen willst Du spielen? \n(1) ein anderer Spieler \n(2) KI"
var game_text: String = "(R) für Neustart"

@onready var points_left_label: Label = $"PointsLeft"
@onready var points_right_label: Label = $"PointsRight"
@onready var info_screen = $"InfoScreen"


func _ready() -> void:
	info_screen.text = start_text


func update_points():
	points_left_label.text = str(points_left)
	points_right_label.text = str(points_right)


func _unhandled_key_input(event: InputEvent) -> void:
	if event.is_action_pressed("reset"):
		reset.emit()
	elif event.is_action_pressed("player_game"):
		start.emit(0)
	elif event.is_action_pressed("ai_game"):
		start.emit(1)


func _on_ball_right_exit() -> void:
	points_left += 1
	update_points()


func _on_ball_left_exit() -> void:
	points_right += 1
	update_points()


func _on_reset() -> void:
	points_left = 0
	points_right = 0
	update_points()
	info_screen.text = start_text


func _on_start(_player) -> void:
	info_screen.text = game_text
