extends CharacterBody2D

enum Functions { PLAYER_ONE, PLAYER_TWO, AI, NULL }

@export var colour: Color
@export var start_position: Vector2

var function: int
var image: Rect2 = Rect2(-10, -40, 20, 80)
var speed = 600

@onready var ball_node: CharacterBody2D = $"../Ball"


func _ready() -> void:
	match name:
		"PaddleLeft":
			start_position = Vector2(12, 302)
			colour = Color("d08ae9")
		"PaddleRight":
			start_position = Vector2(1135, 287)
			colour = Color("3ab9a5")
	reset()


func _draw() -> void:
	draw_rect(image, colour)


func _unhandled_key_input(event: InputEvent) -> void:
	match function:
		0:
			if event.is_action_pressed("left_up"):
				velocity = Vector2.UP * speed
			elif event.is_action_pressed("left_down"):
				velocity = Vector2.DOWN * speed
			elif event.is_action_released("left_down") || event.is_action_released("left_up"):
				velocity = Vector2.ZERO
		1:
			if event.is_action_pressed("right_up"):
				velocity = Vector2.UP * speed
			elif event.is_action_pressed("right_down"):
				velocity = Vector2.DOWN * speed
			elif event.is_action_released("right_down") || event.is_action_released("right_up"):
				velocity = Vector2.ZERO


func _physics_process(delta):
	if function == Functions.AI:
		velocity.y = clamp(
			global_position.direction_to(ball_node.global_position).y * speed, -100, 100
		)
		prints(velocity.y)
	var collision_info = move_and_collide(velocity * delta)
	if collision_info:
		velocity = Vector2.ZERO


func reset():
	if name == "PaddleLeft":
		function = Functions.PLAYER_ONE
	elif name == "PaddleRight":
		function = Functions.PLAYER_TWO
	position = start_position
	velocity = Vector2.ZERO


func _on_main_scene_reset() -> void:
	reset()


func _on_main_scene_start(_control: int) -> void:
	if _control == 1 && name == "PaddleRight":
		function = Functions.AI
