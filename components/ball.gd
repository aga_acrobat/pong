extends CharacterBody2D

signal left_exit
signal right_exit

@export var speed = 600
var image

@onready var window_rect: Rect2 = get_window().get_visible_rect()
@onready var bounce_audio: AudioStreamPlayer2D = $"BounceAudio"
@onready var point_audio: AudioStreamPlayer2D = $"PointAudio"


func _ready() -> void:
	velocity = Vector2.ZERO
	position = window_rect.get_center()


func _draw() -> void:
	draw_circle(Vector2(), 10.0, Color.WHITE)


func _physics_process(delta):
	var collision_info = move_and_collide(velocity * delta)
	if collision_info:
		velocity = velocity.bounce(collision_info.get_normal())
		bounce_audio.play()


func start() -> void:
	rotation = 0
	var random_direction: float = 0.0
	while !random_direction:
		random_direction = randf_range(0, 2 * PI)
		if (
			(random_direction >= 0.3 * PI && random_direction <= 0.6 * PI)
			|| (random_direction >= 1.3 * PI && random_direction <= 1.6 * PI)
		):
			rotate(random_direction)
		else:
			random_direction = 0
	await get_tree().create_timer(1.0).timeout
	velocity = Vector2.UP.rotated(rotation) * speed


func _on_visible_on_screen_notifier_2d_screen_exited() -> void:
	if position.x < window_rect.position.x:
		left_exit.emit()
	elif position.x > window_rect.end.x:
		right_exit.emit()
	point_audio.play()
	velocity = Vector2.ZERO
	position = window_rect.get_center()
	start()


func _on_main_scene_reset() -> void:
	velocity = Vector2.ZERO
	position = window_rect.get_center()
	rotation = 0


func _on_main_scene_start(_player) -> void:
	start()
